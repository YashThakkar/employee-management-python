from tkinter import *
from tkinter import filedialog
# loading Python Imaging Library 
from PIL import ImageTk, Image
from tkinter import ttk #for treeview
import os
from DBClass import *

class MainClass:
	def __init__(self, root):
		self.current_frame = ''
		self.empimg = ""
		self.del_flag = 0
		self.update_flag = 0
		self.optval = 0

		self.root = root
		self.root.geometry('1920x1080')
		self.root.title("Employee Management")

		conn = sqlite3.connect("Employee.db") #creating new database if there it doesn't exists
		c = conn.cursor()
		self.dbc = DBClass(c,conn)
		self.dbc.creating_database()

		# Topbar(black)
		self.topBarFrame = Frame(self.root, height=50, width=1920, bg='#34383C')
		self.topBarFrame.propagate(0)
		self.topBarFrame.place(x=0,y=0)

		#Logo box
		self.topLeftBox = Frame(self.topBarFrame, height=50, width=80, bg="#1E2226")
		self.topLeftBox.place(x=0,y=0)
		self.logo_lbl = Label(self.topLeftBox, text="EM",width=6, pady=0,padx=0, height=2, bg='#1E2226', fg='#6A8F4E', font=('Raleway', 14 ,'bold'))
		self.logo_lbl.place(x=0,y=0)

		#search feild
		self.searchvar = StringVar()
		self.searchbar = Entry(self.topBarFrame, width=20,state=NORMAL,textvariable = self.searchvar, fg='#909394', bg='#464A4D',bd=0, font=('Raleway', 12))
		self.searchbar.bind("<FocusIn>", self.clearSearchField) #single mouse click
		self.searchbar.bind("<Return>", self.searchAction) #when enter is clicked
		self.searchbar.insert(0,'Search')
		self.searchbar.place(x=1100, y=13)

		self.searchOptionvar = IntVar()
		self.genderstr = ''
		self.searchOption_lbl = Label(self.topBarFrame, text='Search By: ', fg='#ccc', bg='#34383C', font=('Raleway', 13)).place(x=790, y=13)
		self.nameOption = Radiobutton(self.topBarFrame, bg="#34383C", fg="#ccc",activebackground='#34383C',activeforeground='#91C46B', font=('Raleway', 13), text="Name", variable=self.searchOptionvar, value=1, command=self.setsearchOption)
		self.idOption = Radiobutton(self.topBarFrame, bg="#34383C", fg="#ccc",activeforeground='#91C46B', font=('Raleway', 13), text="Id", variable=self.searchOptionvar, value=2, command=self.setsearchOption)
		self.nameOption.place(x=900, y=13)
		self.idOption.place(x=1000, y=13)

		#search Cancel button
		self.cancelImg = PhotoImage(file='images/close.png')
		self.searchCancellbl = Label(self.topBarFrame,width=50,height=35,cursor='hand2',bg='#34383C',image=self.cancelImg)
		self.searchCancellbl.bind('<Button-1>', self.restoreTable)

		# Left frame(Green)
		self.leftFrame = Frame(self.root, height=1010, width=80, bg='#91C46B')
		self.leftFrame.propagate(0)
		self.leftFrame.place(x=0,y=50)

		#add
		self.addIcon = PhotoImage(file='images/add-black.png')
		self.add_lbl = Label(self.leftFrame,width= 80, height=60,cursor='hand2', padx=0, image=self.addIcon, bg='#91C46B')
		self.add_lbl.bind('<Enter>', self.addHoverEnter)
		self.add_lbl.bind('<Leave>', self.addHoverLeave)
		self.add_lbl.bind("<Button-1>", self.addEmployee)
		self.add_lbl.place(x=0,y=100)

		#Home
		self.homeIcon = PhotoImage(file='images/home-black.png')
		self.home_lbl = Label(self.leftFrame,width= 80, height=60,cursor='hand2', padx=0, image=self.homeIcon, bg='#91C46B')
		self.home_lbl.bind('<Enter>', self.homeHoverEnter)
		self.home_lbl.bind('<Leave>', self.homeHoverLeave)
		self.home_lbl.bind("<Button-1>", self.homeClicked)
		self.home_lbl.place(x=0,y=200)

		#Delete
		self.deleteIcon = PhotoImage(file='images/delete-black.png')
		self.delete_lbl = Label(self.leftFrame,width= 80, height=60,cursor='hand2', padx=0, image=self.deleteIcon, bg='#91C46B')
		self.delete_lbl.bind('<Enter>', self.deleteHoverEnter)
		self.delete_lbl.bind('<Leave>', self.deleteHoverLeave)
		self.delete_lbl.bind("<Button-1>", self.deleteClicked)
		self.delete_lbl.place(x=0,y=300)
	
		#Update
		self.updateIcon = PhotoImage(file='images/update-black.png')
		self.update_lbl = Label(self.leftFrame,width= 80, height=60,cursor='hand2', padx=0, image=self.updateIcon, bg='#91C46B')
		self.update_lbl.bind('<Enter>', self.updateHoverEnter)
		self.update_lbl.bind('<Leave>', self.updateHoverLeave)
		self.update_lbl.bind("<Button-1>", self.showUpdateframe)
		self.update_lbl.place(x=0,y=400)		

		# Details Frame
		self.detailsFrame = Frame(self.root, height=1030, width=1840, bg='white')
		#self.detailsFrame.place(x=80,y=50)

		self.idvar = IntVar() 
		self.id_lbl = Label(self.detailsFrame, text='Emp ID: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=80, y=80)
		self.id_e = Entry(self.detailsFrame,textvariable=self.idvar,  width=20, bd=0, fg='#464A4D', bg='white', font=('Raleway', 13))
		self.id_e.place(x=150,y=80)
		self.id_line_frame = Frame(self.detailsFrame, width=200,height=1,bg='#91C46B').place(x=150, y=102)

		self.namevar = StringVar()
		self.name_lbl = Label(self.detailsFrame, text='Name: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=80, y=180)
		self.name_e = Entry(self.detailsFrame, width=20, bd=0, textvariable=self.namevar, fg='#ccc', bg='white', font=('Raleway', 13))
		self.name_e.bind("<FocusIn>", self.clearNameField)
		self.name_e.insert(0,'Enter Name')
		self.name_e.place(x=150,y=180)
		self.name_line_frame = Frame(self.detailsFrame, width=200,height=1,bg='#91C46B').place(x=150, y=202)

		self.dobvar = StringVar()
		self.dob_lbl = Label(self.detailsFrame, text='Dob: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=80, y=280)
		self.dob_e = Entry(self.detailsFrame, width=20, bd=0, textvariable=self.dobvar, fg='#ccc', bg='white', font=('Raleway', 13))
		self.dob_e.bind("<FocusIn>", self.clearDobField)
		self.dob_e.insert(0,'dd/mm/yyyy')
		self.dob_e.place(x=150,y=280)
		self.dob_line_frame = Frame(self.detailsFrame, width=200,height=1,bg='#91C46B').place(x=150, y=302)

		self.address_lbl = Label(self.detailsFrame, text='Address: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=80, y=380)
		self.address_e = Text(self.detailsFrame,width=30,height=5, bd=1, wrap=WORD, fg='#ccc', bg='white', font=('Raleway', 13))
		self.address_e.bind("<FocusIn>", self.clearAddressField)
		self.address_e.insert(END,'Enter Address...')
		self.address_e.place(x=160,y=380)

		self.deptvar = StringVar()
		self.dept_lbl = Label(self.detailsFrame, text='Department: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=80, y=550)
		self.dept_e = Entry(self.detailsFrame, width=20, bd=0, textvariable=self.deptvar, fg='#ccc', bg='white', font=('Raleway', 13))
		self.dept_e.bind("<FocusIn>", self.clearDeptField)
		self.dept_e.insert(0,'Department')
		self.dept_e.place(x=190,y=550)
		self.dept_line_frame = Frame(self.detailsFrame, width=200,height=1,bg='#91C46B').place(x=190, y=572)

		self.desigvar = StringVar()
		self.desig_lbl = Label(self.detailsFrame, text='Designation: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=80, y=650)
		self.desig_e = Entry(self.detailsFrame, width=20, bd=0, textvariable=self.desigvar, fg='#ccc', bg='white', font=('Raleway', 13))
		self.desig_e.bind("<FocusIn>", self.clearDesigField)
		self.desig_e.insert(0,'Designation')
		self.desig_e.place(x=190,y=650)
		self.desig_line_frame = Frame(self.detailsFrame, width=200,height=1,bg='#91C46B').place(x=190, y=672)

		self.agevar = IntVar()
		self.age_lbl = Label(self.detailsFrame, text='Age: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=550, y=80)
		self.age_e = Entry(self.detailsFrame, width=20, bd=0, textvariable=self.agevar, fg='#ccc', bg='white', font=('Raleway', 13))
		self.age_e.bind("<FocusIn>", self.clearAgeField)
		self.age_e.insert(0,'Age')
		self.age_e.place(x=600,y=80)
		self.age_line_frame = Frame(self.detailsFrame, width=200,height=1,bg='#91C46B').place(x=600, y=102)

		self.gendervar = IntVar()
		self.genderstr = ''
		self.sal_lbl = Label(self.detailsFrame, text='Gender: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=550, y=180)
		self.malebtn = Radiobutton(self.detailsFrame, bg="white", fg="#464A4D",activeforeground='#91C46B', font=('Raleway', 13), text="Male", variable=self.gendervar, value=1, command=self.setGender)
		self.femalebtn = Radiobutton(self.detailsFrame, bg="white", fg="#464A4D",activeforeground='#91C46B', font=('Raleway', 13), text="Female", variable=self.gendervar, value=2, command=self.setGender)
		self.malebtn.place(x=630, y=180)
		self.femalebtn.place(x=710, y=180)

		self.salvar = IntVar()
		self.sal_lbl = Label(self.detailsFrame, text='Salary: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=550, y=280)
		self.sal_e = Entry(self.detailsFrame, width=20, bd=0, textvariable=self.salvar, fg='#ccc', bg='white', font=('Raleway', 13))
		self.sal_e.bind("<FocusIn>", self.clearSalField)
		self.sal_e.insert(0,'Salary')
		self.sal_e.place(x=610,y=280)
		self.sal_line_frame = Frame(self.detailsFrame, width=200,height=1,bg='#91C46B').place(x=610, y=302)

		self.emailvar = StringVar()
		self.email_lbl = Label(self.detailsFrame, text='Email: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=550, y=380)
		self.email_e = Entry(self.detailsFrame, width=20, bd=0, textvariable=self.emailvar, fg='#ccc', bg='white', font=('Raleway', 13))
		self.email_e.bind("<FocusIn>", self.clearEmailField)
		self.email_e.insert(0,'Email')
		self.email_e.place(x=610,y=380)
		self.email_line_frame = Frame(self.detailsFrame, width=200,height=1,bg='#91C46B').place(x=610, y=402)

		self.contactvar = StringVar()
		self.contact_lbl = Label(self.detailsFrame, text='Contact: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=550, y=480)
		self.contact_e = Entry(self.detailsFrame, width=20, bd=0, textvariable=self.contactvar, fg='#ccc', bg='white', font=('Raleway', 13))
		self.contact_e.bind("<FocusIn>", self.clearContactField)
		self.contact_e.insert(0,'Contact number')
		self.contact_e.place(x=622,y=480)
		self.contact_line_frame = Frame(self.detailsFrame, width=200,height=1,bg='#91C46B').place(x=622, y=502)

		self.joiningdatevar = StringVar()
		self.joiningdate_lbl = Label(self.detailsFrame, text='Joining Date: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=550, y=580)
		self.joiningdate_e = Entry(self.detailsFrame, width=20, bd=0, textvariable=self.joiningdatevar, fg='#ccc', bg='white', font=('Raleway', 13))
		self.joiningdate_e.bind("<FocusIn>", self.clearJoiningdateField)
		self.joiningdate_e.insert(0,'dd/mm/yyyy')
		self.joiningdate_e.place(x=660,y=580)
		self.joiningdate_line_frame = Frame(self.detailsFrame, width=200,height=1,bg='#91C46B').place(x=660, y=602)

		self.imgpathvar = StringVar()
		self.img_lbl = Label(self.detailsFrame, text='Image: ', fg='#464A4D', bg='white', font=('Raleway', 13)).place(x=950, y=80)
		self.imgpath_e = Entry(self.detailsFrame, width=20, bd=0, textvariable=self.imgpathvar, fg='#ccc', bg='white', font=('Raleway', 13))
		#self.imgpath_e.bind("<FocusIn>", self.clearSalField)
		self.imgpath_e.insert(0,'Image path')
		self.imgpath_e.place(x=1020,y=80)
		self.imgpath_line_frame = Frame(self.detailsFrame, width=200,height=1,bg='#91C46B').place(x=1020, y=112)

		self.select_img_btn = Button(self.detailsFrame,bd=0, text='Select Image',bg='#91C46B',fg='#464A4D',width=30, height=2,activebackground='#6A8F4E',activeforeground='white', command=self.selectImage).place(x=1030,y=130)
		self.selected_img = Label(self.detailsFrame, bg='lightgray',width=30, height=15)
		self.selected_img.place(x=1030,y=180)

		# Submit button
		#self.updatebtn = Button(self.detailsFrame,bd=1, text='Update',justify=CENTER,bg='#34383C',fg='white',width=40, height=2,activebackground='#585C5F',activeforeground='#91C46B', command=self.updateBtnAction).place(x=1000,y=450)

		# Submit button
		self.submitbtn = Button(self.detailsFrame,bd=1,cursor='hand2',justify=CENTER,text="Submit",bg='#34383C',fg='white',width=40, height=2,activebackground='#585C5F',activeforeground='#91C46B')
		self.submitbtn.place(x=1000,y=450)
		self.submitbtn.bind("<Button-1>", self.submitBtnAction)

		
		#self.updatebtn.place_forget() #initially invisible
		#----Display Frame----
		self.displayFrame = Frame(self.root, height=1030, width=1840, bg='white')
		self.displayFrame.place(x=80,y=50)
		self.current_frame = 'displayFrame'

		#search status label 
		self.searchStatus_lbl = Label(self.displayFrame, text='', fg='red', bg='white', font=('Raleway', 13 ,'bold'))
		self.searchStatus_lbl.place(x=1000, y=12)
		

		# self.treev.insert("", 'end', text ="L1",  
  #            values =("yash", "27/11/2000", "25","Male","This is my addressbkbvabd vavbja","Comp","CEO","328943","yash@g.com","90348382","22/4/2020","images/sky.jpg")) 
		
		self.treeOperations()
		
		#Deleting frame
		self.deleteFrame = Frame(self.displayFrame,height=150,width=1840,bg='#34383C')
		self.deleteFrame.place(x=0,y=630)
		self.deletevar = IntVar()
		self.delete_frame_lbl = Label(self.deleteFrame, text='Employ Id: ', fg='white', bg='#34383C', font=('Raleway', 13)).place(x=50, y=50)
		self.delete_e = Entry(self.deleteFrame, width=23, bd=0, textvariable=self.deletevar, fg='#ccc', bg='#34383C', font=('Raleway', 13))
		self.delete_e.bind("<FocusIn>", self.clearDeleteField)
		self.delete_e.insert(0,'Employ ID to be deleted')
		self.delete_e.place(x=170,y=50)
		self.delete_line_frame = Frame(self.deleteFrame, width=220,height=2,bg='#91C46B').place(x=170, y=73)

		self.deleteBtn = Button(self.deleteFrame,bd=1,justify=CENTER,cursor='hand2',text="Delete",bg='#91C46B',fg='#34383C',width=20, height=2,activebackground='#6A8F4E',activeforeground='white')
		self.deleteBtn.place(x=410,y=40)
		self.deleteBtn.bind('<Button-1>',self.deleteBtnClicked)

		self.deleting_gif = Label(self.deleteFrame,width=150,height=140, bg='#34383C', font=('Raleway', 13))
		self.deleting_gif.place(x=600, y=0)
		self.deleting_gif.place_forget()

		self.del_status_lbl = Label(self.deleteFrame,width=30, text='', fg='#34383C', bg='#91C46B', font=('Raleway', 13))
		self.del_status_lbl.place(x=600, y=40)
		self.del_status_lbl.place_forget()
		
		self.deleteFrame.place_forget()

		#Updating frame
		self.updateFrame = Frame(self.displayFrame,height=150,width=1840,bg='#34383C')
		self.updateFrame.place(x=0,y=630)
		self.updatevar = IntVar()
		self.update_frame_lbl = Label(self.updateFrame, text='Employ Id: ', fg='white', bg='#34383C', font=('Raleway', 13)).place(x=50, y=50)
		self.update_e = Entry(self.updateFrame, width=23, bd=0, textvariable=self.updatevar, fg='#ccc', bg='#34383C', font=('Raleway', 13))
		self.update_e.bind("<FocusIn>", self.clearUpdateField)
		self.update_e.insert(0,'Employ ID to be updated')
		self.update_e.place(x=170,y=50)
		self.update_line_frame = Frame(self.updateFrame, width=220,height=2,bg='#91C46B').place(x=170, y=73)

		self.updateBtn = Button(self.updateFrame,bd=1,justify=CENTER,cursor='hand2',text="Update",bg='#91C46B',fg='#34383C',width=20, height=2,activebackground='#6A8F4E',activeforeground='white')
		self.updateBtn.place(x=410,y=40)
		self.updateBtn.bind('<Button-1>',self.updateBtnClicked)
		
		self.updateFrame.place_forget()


	def treeOperations(self):

		self.treev = ttk.Treeview(self.displayFrame, selectmode ='browse',height=25)
		# self.treev.tag_configure('odd', background='cyan')
		# self.treev.tag_configure('even', background='#DFDFDF')
		self.treev.place(x=125,y=50)
		verscrlbar = ttk.Scrollbar(self.treev, orient = VERTICAL, command = self.treev.yview)
		# verscrlbar.pack(side =RIGHT, fill =Y) 
		verscrlbar.place(x=1175,y=1,height=525)
		self.treev.config(yscrollcommand = verscrlbar.set)

		self.treev["columns"] = ("0", "1", "2","3","4","5","6","7","8","9","10","11","12") 
		self.treev['show'] = 'headings'
		self.treev.column("0", width = 50, anchor ='c')
		self.treev.column("1", width = 90, anchor ='c')
		self.treev.column("2", width = 90, anchor ='c')
		self.treev.column("3", width = 90, anchor ='c')
		self.treev.column("4", width = 90, anchor ='c')
		self.treev.column("5", width = 130, anchor ='c')
		self.treev.column("6", width = 90, anchor ='c')
		self.treev.column("7", width = 90, anchor ='c')
		self.treev.column("8", width = 90, anchor ='c')
		self.treev.column("9", width = 110, anchor ='c')
		self.treev.column("10", width = 90, anchor ='c')
		self.treev.column("11", width = 90, anchor ='c')
		self.treev.column("12", width = 90, anchor ='c')

		# respective columns
		self.treev.heading("0", text ="Emp ID") 
		self.treev.heading("1", text ="Name") 
		self.treev.heading("2", text ="DOB") 
		self.treev.heading("3", text ="Age")
		self.treev.heading("4", text ="Gender")
		self.treev.heading("5", text ="Address")
		self.treev.heading("6", text ="Department")
		self.treev.heading("7", text ="Designation")
		self.treev.heading("8", text ="Salary")
		self.treev.heading("9", text ="Email")
		self.treev.heading("10", text ="Contact")
		self.treev.heading("11", text ="Joining Date")
		self.treev.heading("12", text ="Image") 

		self.treev.bind("<Button-1>", self.treeSingleClick)

		self.treev.bind("<Double-1>", self.treeClick)

		count = self.dbc.get_rowCount()
		print("count= ",count)
		color_flag = 'odd'
		if(count != 0):
			mainrow = self.dbc.get_data()
			len_mainrow = len(mainrow)
			i=0
			while(i < len_mainrow):
				row = mainrow[i]
				photoPath = "dbImages/"+((str)(row[0]))+".jpg"
				newfile = self.writeToFile(row[12],photoPath)
				newfile_name = ((str)(row[0]))+".jpg"
				#print("newfile: ",newfile)
				self.treev.insert("",'end',text="L1",
					values=(row[0],row[1],row[2],row[6],row[7],row[3],row[4],row[5],row[8],row[9],row[10],row[11],newfile_name))#, tags=('odd'))
				i += 1
				# if(color_flag == 'odd'):
				# 	color_flag = 'even'
				# else:
				# 	color_flag = 'odd'

	def treeSingleClick(self,event):
		item = self.treev.selection()
		print("item: ",item)
		for i in item:
			print("you clicked on: ", self.treev.item(i, "values")[0])
			# self.treeIndex = self.treev.item(i, "values")[0]

	def writeToFile(self,data, filename):
		#convert binary to proper format and storing in given folder project folder
		with open(filename, 'wb') as file:
			file.write(data)
		return file

	def readyAllTextfields(self):
		#changing font color
		self.id_e.config(foreground="#464A4D")
		self.name_e.config(foreground="#464A4D")
		self.dob_e.config(foreground="#464A4D")
		self.address_e.config(foreground="#464A4D")
		self.desig_e.config(foreground="#464A4D")
		self.dept_e.config(foreground="#464A4D")
		self.sal_e.config(foreground="#464A4D")
		self.email_e.config(foreground="#464A4D")
		self.contact_e.config(foreground="#464A4D")
		self.age_e.config(foreground="#464A4D")
		self.joiningdate_e.config(foreground="#464A4D")
		self.imgpath_e.config(foreground="#464A4D")

		#deleting placeholder text
		self.id_e.delete(0,END)
		self.name_e.delete(0,END)
		self.dob_e.delete(0,END)
		self.address_e.delete('1.0',END)
		self.desig_e.delete(0,END)
		self.dept_e.delete(0,END)
		self.sal_e.delete(0,END)
		self.email_e.delete(0,END)
		self.contact_e.delete(0,END)
		self.age_e.delete(0,END)
		self.joiningdate_e.delete(0,END)
		self.imgpath_e.delete(0,END)

	def treeClick(self,event):
		self.current_frame = 'updateFrame'
		self.displayFrame.place_forget()
		self.detailsFrame.place(x=80,y=50)
		self.submitbtn.config(text="Update")
		# self.submitbtn.place_forget()
		# self.updatebtn.place(x=1000,y=450)

		item = self.treev.selection()
		print("item: ",item)
		for i in item:
			print("you clicked on: ", self.treev.item(i, "values")[0])
			self.treeIndex = self.treev.item(i, "values")[0]

		self.displayFrame.place_forget()
		self.detailsFrame.place(x=80,y=50)

		maininfo = self.dbc.get_specific_data(self.treeIndex)
		print("maininfo: ",maininfo[0])

		self.readyAllTextfields()

		got_id = (str)(maininfo[0])
		self.id_e.insert(0,got_id)
		self.name_e.insert(0,maininfo[1])
		self.dob_e.insert(0,maininfo[2])
		self.address_e.insert(1.0,maininfo[3])
		self.dept_e.insert(0,maininfo[4])
		self.desig_e.insert(0,maininfo[5])
		self.age_e.insert(0,(str)(maininfo[6]))
		db_gender = maininfo[7]
		if(db_gender == 'Male'):
			gen_id = 1
			self.gendervar.set(1)
			self.femalebtn.config(foreground="#464A4D")
			self.malebtn.config(foreground="#91C46B")
		elif(db_gender == 'Female'):
			gen_id = 2
			self.gendervar.set(2)
			self.malebtn.config(foreground="#464A4D")
			self.femalebtn.config(foreground="#91C46B")
		# self.setGender
		#self.ge.insert(0,self.genderstr)
		self.sal_e.insert(0,(str)(maininfo[8]))
		self.email_e.insert(0,maininfo[9])
		print("contct: ",maininfo[10])
		self.contact_e.insert(0,(str)(maininfo[10]))
		self.joiningdate_e.insert(0,maininfo[11])

		self.ip = 'dbImages/'+got_id+'.jpg'
		self.imgpath_e.insert(0,self.ip)
		f = Image.open(self.ip)
		#f.show()
		img = f.resize((215,250),Image.ANTIALIAS)
		self.empimg = ImageTk.PhotoImage(img) 
		self.selected_img.config(image=self.empimg)
		self.selected_img.config(width=215,height=250)

	def clearSearchField(self,event):
			self.searchbar.delete(0,END)
			
	def clearNameField(self,event):
			self.name_e.delete(0,END)
			self.name_e.config(foreground="#464A4D")

	def clearDobField(self,event):
			self.dob_e.delete(0,END)
			self.dob_e.config(foreground="#464A4D")

	def clearAddressField(self,event):
			self.address_e.delete('1.0',END)
			self.address_e.config(foreground="#464A4D")

	def clearDeptField(self,event):
			self.dept_e.delete(0,END)
			self.dept_e.config(foreground="#464A4D")

	def clearDesigField(self,event):
		self.desig_e.delete(0,END)
		self.desig_e.config(foreground="#464A4D")

	def clearAgeField(self,event):
		self.age_e.delete(0,END)
		self.age_e.config(foreground="#464A4D")

	def clearSalField(self,event):
		self.sal_e.delete(0,END)
		self.sal_e.config(foreground="#464A4D")

	def clearEmailField(self,event):
		self.email_e.delete(0,END)
		self.email_e.config(foreground="#464A4D")

	def clearContactField(self,event):
		self.contact_e.delete(0,END)
		self.contact_e.config(foreground="#464A4D")

	def clearJoiningdateField(self,event):
		self.joiningdate_e.delete(0,END)
		self.joiningdate_e.config(foreground="#464A4D")

	def clearDeleteField(self,event):
		self.delete_e.delete(0,END)
		self.delete_e.config(foreground="white")

	def clearUpdateField(self,event):
		self.update_e.delete(0,END)

	#----------Add methods-----------
	def addHoverEnter(self,event):
		self.add_lbl.config(background="#34383C")
		self.addIcon = PhotoImage(file='images/add-green.png')
		self.add_lbl.config(image=self.addIcon)

	def addHoverLeave(self,event):
		self.add_lbl.config(background="#91C46B")
		self.addIcon = PhotoImage(file='images/add-black.png')
		self.add_lbl.config(image=self.addIcon)

	def addEmployee(self,event):
		if(self.current_frame == 'displayFrame'):
			self.displayFrame.place_forget()
			self.detailsFrame.place(x=80,y=50)
			self.current_frame = 'detailsFrame'
			# self.updatebtn.place_forget()
			# self.submitbtn.place(x=1000,y=450)
			# self.current_frame = 'detailsFrame'
		if(self.current_frame == 'updateFrame'):
			self.displayFrame.place_forget()
			self.detailsFrame.place(x=80,y=50)
			self.current_frame = 'detailsFrame'
			#---
		self.submitbtn.config(text="Submit")
		self.selected_img.config(width=30, height=15)

		self.id_e.delete(0,END)
		self.name_e.delete(0,END)
		self.dob_e.delete(0,END)
		self.address_e.delete('1.0',END)
		self.dept_e.delete(0,END)
		self.desig_e.delete(0,END)
		self.age_e.delete(0,END)
		self.email_e.delete(0,END)
		self.contact_e.delete(0,END)
		self.sal_e.delete(0,END)
		self.gendervar.set(None)
		self.joiningdate_e.delete(0,END)
		self.imgpath_e.delete(0,END)

		self.id_e.config(fg='#ccc')
		self.name_e.config(fg='#ccc')
		self.dob_e.config(fg='#ccc')
		self.address_e.config(fg='#ccc')
		self.dept_e.config(fg='#ccc')
		self.desig_e.config(fg='#ccc')
		self.age_e.config(fg='#ccc')
		self.email_e.config(fg='#ccc')
		self.contact_e.config(fg='#ccc')
		self.sal_e.config(fg='#ccc')
		self.malebtn.config(fg='#464A4D')
		self.femalebtn.config(fg='#464A4D')
		self.joiningdate_e.config(fg='#ccc')
		self.selected_img.config(image='')

		self.id_e.insert(0,'Employ Id')
		self.name_e.insert(0,'Enter name')
		self.dob_e.insert(0,"Enter DOB")
		self.address_e.insert('1.0','Enter Address')
		self.dept_e.insert(0,"Enter department")
		self.desig_e.insert(0,"Enter designation")
		self.age_e.insert(0,"Enter age")
		self.email_e.insert(0,"email")
		self.contact_e.insert(0,"Enter contact no.")
		self.sal_e.insert(0,"Enter salary")
		self.joiningdate_e.insert(0,'joining date')
		
		self.id_e.config(fg='#ccc')

	def updateBtnAction(self):
		print("Inside Update")
		self.idint = (int)(self.idvar.get())
		self.namestr = self.namevar.get()
		self.dobstr = self.dobvar.get()
		self.addressstr = self.address_e.get(1.0,END)
		self.deptstr = self.deptvar.get()
		self.desigstr = self.desigvar.get()
		self.ageint = (int)(self.agevar.get())
		self.setGender()
		self.salary = (int)(self.salvar.get())
		self.email = self.emailvar.get()
		self.contact = self.contactvar.get()
		self.joining = self.joiningdatevar.get()

		# self.imgpath = 'dbImages/'+(str)(self.idint)+'.jpg'
		# f = Image.open(self.imgpath)
		# # f.show()
		# img = f.resize((1000,1000),Image.ANTIALIAS)
		# self.empimg = ImageTk.PhotoImage(img) 
		# self.selected_img.config(image=self.eimg)

		# self.image = self.empimg

		self.dbc.update_database(self.idint,self.namestr,self.dobstr, self.addressstr, self.deptstr, self.desigstr,
			self.ageint, self.genderstr, self.salary, self.email, self.contact, self.joining, self.imgpath_e.get())

		#Going on home page automatically
		
		
		if(self.current_frame == 'updateFrame'):
			self.detailsFrame.place_forget()
			self.displayFrame.place(x=80,y=50)
			self.treeOperations()
			current_frame = 'displayFrame'


	#Home methods
	def homeHoverEnter(self,event):
		self.home_lbl.config(background="#34383C")
		self.homeIcon = PhotoImage(file='images/home-green.png')
		self.home_lbl.config(image=self.homeIcon)

	def homeHoverLeave(self,event):
		self.home_lbl.config(background="#91C46B")
		self.homeIcon = PhotoImage(file='images/home-black.png')
		self.home_lbl.config(image=self.homeIcon)

	def homeClicked(self,event):
		if(self.current_frame == 'detailsFrame'):
			self.detailsFrame.place_forget()
			self.displayFrame.place(x=80,y=50)
			self.current_frame = 'displayFrame'
			self.treeOperations()
		if(self.current_frame == 'updateFrame'):
			self.detailsFrame.place_forget()
			self.displayFrame.place(x=80,y=50)
			self.current_frame = 'displayFrame'
			self.treeOperations()

		self.searchStatus_lbl.config(text="")
		#search status will be refreshed
		self.restore_search_status()

		self.deleteFrame.place_forget()
		self.updateFrame.place_forget()


	#Delete Methods
	def deleteHoverEnter(self,event):
		self.delete_lbl.config(background="#34383C")
		self.deleteIcon = PhotoImage(file='images/delete-green.png')
		self.delete_lbl.config(image=self.deleteIcon)

	def deleteHoverLeave(self,event):
		self.delete_lbl.config(background="#91C46B")
		self.deleteIcon = PhotoImage(file='images/delete-black.png')
		self.delete_lbl.config(image=self.deleteIcon)

	def deleteClicked(self,event):
		if((self.current_frame == 'displayFrame') and (self.del_flag == 0)):
			self.deleteFrame.place(x=0,y=630)
			self.del_flag = 1
		elif(self.del_flag == 1):
			self.deleteFrame.place_forget()
			self.del_status_lbl.place_forget()
			self.del_flag = 0

	def removeDeletingGif(self):
		self.deleting_gif.place_forget()
		self.del_status_lbl.place(x=600, y=40)
		self.del_status_lbl.after(3000, lambda : self.del_status_lbl.place_forget( ))
		self.dbc.deleting_data(self.delInt)
		self.treeOperations()
		self.del_status_lbl.config(text="Employee Id: "+self.delstr+" successfully")

	def deleteBtnClicked(self,event):
		self.delstr = (str)(self.deletevar.get())
		self.delInt = (int)(self.delstr)
		if(self.delstr != ''):	
			print("Id that will be deleted: ",self.delInt)
			self.deleting_gif.place(x=600,y=0)
			self.deleteGif = PhotoImage(file='images/deleting-gif2.gif')
			self.deleting_gif.config(image=self.deleteGif)
			self.deleting_gif.after(3000,self.removeDeletingGif)
			#Deleting image file from dbImages folder
			if os.path.exists("dbImages/"+self.delstr+".jpg"):
			  os.remove("dbImages/"+self.delstr+".jpg")
			else:
			  pass
		else:
			pass

	def setGender(self):
		rval = self.gendervar.get()
		self.genderstr = ''
		if(rval == 1):
			self.genderstr = 'Male'
			self.femalebtn.config(foreground="#464A4D")
			self.malebtn.config(foreground="#91C46B")
		if(rval == 2):
			self.genderstr = 'Female'
			self.malebtn.config(foreground="#464A4D")
			self.femalebtn.config(foreground="#91C46B")

	#Update methods 
	def updateHoverEnter(self,event):
		self.update_lbl.config(background="#34383C")
		self.updateIcon = PhotoImage(file='images/update-green.png')
		self.update_lbl.config(image=self.updateIcon)

	def updateHoverLeave(self,event):
		self.update_lbl.config(background="#91C46B")
		self.updateIcon = PhotoImage(file='images/update-black.png')
		self.update_lbl.config(image=self.updateIcon)

	def showUpdateframe(self,event):
		if((self.current_frame == 'displayFrame') and (self.update_flag == 0)):
			self.updateFrame.place(x=0,y=630)
			self.update_flag = 1
		elif(self.update_flag == 1):
			self.updateFrame.place_forget()
			self.update_flag = 0

	def updateBtnClicked(self,event):
		self.upstr = (str)(self.updatevar.get())
		self.upInt = (int)(self.upstr)
		if(self.upstr != ''):	
			print("Id that will be updated: ",self.upInt)
			self.updateButton(self.upInt)

	def updateButton(self,empid):
		self.current_frame = 'updateFrame'
		self.displayFrame.place_forget()
		self.detailsFrame.place(x=80,y=50)
		self.submitbtn.config(text="Update")
		# self.submitbtn.place_forget()
		# self.updatebtn.place(x=1000,y=450)

		self.update_id = empid
		self.displayFrame.place_forget()
		self.detailsFrame.place(x=80,y=50)

		maininfo = self.dbc.get_specific_data(self.update_id)
		print("maininfo: ",maininfo[0])

		self.readyAllTextfields()

		got_id = (str)(maininfo[0])
		self.id_e.insert(0,got_id)
		self.name_e.insert(0,maininfo[1])
		self.dob_e.insert(0,maininfo[2])
		self.address_e.insert(1.0,maininfo[3])
		self.dept_e.insert(0,maininfo[4])
		self.desig_e.insert(0,maininfo[5])
		self.age_e.insert(0,(str)(maininfo[6]))
		db_gender = maininfo[7]
		if(db_gender == 'Male'):
			gen_id = 1
			self.gendervar.set(1)
			self.femalebtn.config(foreground="#464A4D")
			self.malebtn.config(foreground="#91C46B")
		elif(db_gender == 'Female'):
			gen_id = 2
			self.gendervar.set(2)
			self.malebtn.config(foreground="#464A4D")
			self.femalebtn.config(foreground="#91C46B")
		# self.setGender
		#self.ge.insert(0,self.genderstr)
		self.sal_e.insert(0,(str)(maininfo[8]))
		self.email_e.insert(0,maininfo[9])
		print("contct: ",maininfo[10])
		self.contact_e.insert(0,(str)(maininfo[10]))
		self.joiningdate_e.insert(0,maininfo[11])

		self.ip = 'dbImages/'+got_id+'.jpg'
		self.imgpath_e.insert(0,self.ip)
		f = Image.open(self.ip)
		#f.show()
		img = f.resize((215,250),Image.ANTIALIAS)
		self.empimg = ImageTk.PhotoImage(img) 
		self.selected_img.config(image=self.empimg)
		self.selected_img.config(width=215,height=250)

	# Image select method
	def selectImage(self):
		self.filename = filedialog.askopenfilename(parent=self.root, title='Select a file', filetypes=(("JPEG files", "*.jpeg"),("jpg files",".jpg")))
		if(self.filename != None):
			print("filename: ",self.filename)
			self.imgpath_e.delete(0,END)
			self.imgpath_e.config(foreground="#464A4D")
			self.imgpath_e.insert(0,self.filename)
			f = Image.open(self.filename)
			#f.show()  #open image in external viewer
			img = f.resize((215,250),Image.ANTIALIAS)
			# img.show()
			print("f size: ",f.size)
			print("img size: ",img.size)
			self.empimg = ImageTk.PhotoImage(img) 
			self.selected_img.config(image=self.empimg)
			self.selected_img.config(width=215,height=250)

	def restore_search_status(self):
		self.searchbar.delete(0,END)
		self.searchbar.insert(0,'Search')
		#disable itself i.e. cancel lbl
		self.searchCancellbl.place_forget()

		self.idOption.config(foreground="#ccc")
		self.nameOption.config(foreground="#ccc")
		self.searchOptionvar.set(None)
	
	def restoreTable(self,event):
		#below for loop will delete all previous data from treeview
		for i in self.treev.get_children():
			self.treev.delete(i)

		# fetch all rows from DB and insert into treeview
		self.treeOperations()

		self.restore_search_status()

	def setsearchOption(self):
		self.optval = self.searchOptionvar.get()
		self.soptstr = ''
		if(self.optval == 1):
			#name
			self.idOption.config(foreground="#ccc")
			self.nameOption.config(foreground="#91C46B")
		if(self.optval == 2):
			#id
			self.nameOption.config(foreground="#ccc")
			self.idOption.config(foreground="#91C46B")

	def searchAction(self,event):
		if(self.optval != 0):
			self.searchCancellbl.place(x=1330,y=5)
			if(self.optval == 1):
				#name 
				searchName = self.searchvar.get()
				mainrow = self.dbc.get_data_by_name(searchName)
				len_mainrow = len(mainrow)
				if(len_mainrow != 0):
					self.searchStatus_lbl.config(text="")
					#below for loop will delete all previous data from treeview
					for i in self.treev.get_children():
						self.treev.delete(i)
					i=0
					while(i < len_mainrow):
						row = mainrow[i]
						photoPath = "dbImages/"+((str)(row[0]))+".jpg"
						newfile = self.writeToFile(row[12],photoPath)
						newfile_name = ((str)(row[0]))+".jpg"
						#print("newfile: ",newfile)
						self.treev.insert("",'end',text="L1",
							values=(row[0],row[1],row[2],row[6],row[7],row[3],row[4],row[5],row[8],row[9],row[10],row[11],newfile_name))#, tags=('odd'))
						i += 1
				else:
					print("Name not found !")
					self.searchStatus_lbl.config(text="Name not found !")

			else:
				#id
				rcount = self.dbc.get_rowCount()
				try:
					self.searchID = (int)(self.searchvar.get())
				except ValueError:
					self.searchStatus_lbl.config(text="ID not found !")
				
				if(rcount >= self.searchID):
					print("rcount: ",rcount)
					print("searchid: ",self.searchID)
					self.searchStatus_lbl.config(text="")
					#below for loop will delete all previous data from treeview
					for i in self.treev.get_children():
						self.treev.delete(i)
					row = self.dbc.get_data_by_id(self.searchID)
					img_filename = ((str)(row[0]))+".jpg"
					self.treev.insert("",'end',text="L1",
					values=(row[0],row[1],row[2],row[6],row[7],row[3],row[4],row[5],row[8],row[9],row[10],row[11],img_filename))
				else:
					print("Id not found !")
					self.searchStatus_lbl.config(text="ID not found !")
			# print("Searching...")
		else:
			print("please select option first !")
			self.searchStatus_lbl.config(text="please select option first !")

		
	def submitBtnAction(self,event):
		print("currentframe:- ",self.current_frame)
		if(self.current_frame != 'updateFrame'):
			self.submitbtn.config(text='Submit')
			self.namestr = self.namevar.get()
			self.dobstr = self.dobvar.get()
			self.addressstr = self.address_e.get(1.0,END)
			self.deptstr = self.deptvar.get()
			self.desigstr = self.desigvar.get()
			self.ageint = (int)(self.agevar.get())
			self.salary = (int)(self.salvar.get())
			self.email = self.emailvar.get()
			self.contact = self.contactvar.get()
			self.joining = self.joiningdatevar.get()
			self.image = self.empimg

			print("",self.namestr)
			print("",self.dobstr)
			print("",self.addressstr)
			print("",self.deptstr)
			print("",self.desigstr)
			print("",self.ageint)
			print("",self.genderstr)
			print("",self.salary)
			print("",self.email)
			print("",self.contact)
			print("",self.joining)

			self.dbc.insert_data(self.namestr,self.dobstr, self.addressstr, self.deptstr, self.desigstr,
			self.ageint, self.genderstr, self.salary, self.email, self.contact, self.joining, self.imgpath_e.get())
			#switching frames after click of submit
			self.detailsFrame.place_forget()
			self.treeOperations()
			self.displayFrame.place(x=80,y=50)
			current_frame = 'displayFrame'

		elif(self.current_frame == 'updateFrame'):
			self.submitbtn.config(text='Update')
			print("in else...")
			self.updateFrame.place_forget()
			self.updateBtnAction()
		
root = Tk()
obj = MainClass(root)
root.mainloop()