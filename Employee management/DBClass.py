import sqlite3     #importing SQLite3 package


class DBClass:
	def __init__(self,cur,con):
			self.c = cur
			self.conn = con

	def creating_database(self):
		self.c.execute('''CREATE TABLE IF NOT EXISTS EmpDB(emp_id INT auto_increment primary key, name TEXT, dob TEXT,address TEXT, department TEXT, designation TEXT, age INT, gender TEXT, salary INT, email TEXT, contact CHAR(11), joining TEXT, image BLOB);''')
		print("Table created successfully!")

	def get_rowCount(self):
		try:
			count = 0
			cur = self.c.execute("SELECT * from EmpDB") #storing data in a variable
			for row in cur:
				count +=1
			return count
		except Exception as e:
			print(e, "exception occured!!")

	def convertToBinaryData(self,filename):
	    #Convert digital data to binary format
	    print("update img: "+filename)
	    with open(filename, 'rb') as file:
	        blobData = file.read()
	    return blobData

	def insert_data(self, name, dob, addr, dept, desig, age, gender, sal, email, contact, joining, img):
		try:
			tid = self.get_rowCount()
			tid += 1
			print("tid: ",tid)
			tempimg = self.convertToBinaryData(img)
			#self.c.execute('''INSERT INTO EmpDB VALUES(name, dob, addr, dept, desig, age, gender, sal, email, contact, joining, img)''');
			#c.execute('''INSERT INTO EmpDB VALUES("Jack", 467273, 20, "A/21")''');
			self.c.execute('''INSERT INTO EmpDB VALUES(?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',(tid,name, dob, addr, dept, desig, age, gender, sal, email, contact, joining, tempimg))
			print("Records entered successfully!")
			self.conn.commit()
		except Exception as e:
			print(e, "exception occured!!")
			print("No database created!")
		
	def deleting_data(self, empid):
		empidInt = (str)(empid)
		try:
			self.c.execute('''DELETE from EmpDB where emp_id=?''',(empid,));
			print("Employee Deleted Successfully !")
		except Exception as e:
			print(e, "exception occured!!")
			print("Cannot Delete Database !!!")

	def get_data(self):
		try:
			mainlist = []
			cur = self.c.execute("SELECT * from EmpDB") #storing data in a variable
			for row in cur:
				data_list = [row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12]]
				#return data_list
				mainlist.append(data_list)
			# print("data: ",mainlist)
			return mainlist

			# print("Name= ", row[0])
			print("Operations performed successfully!")
		except Exception as e:
			print(e, "exception occured!!")
			print("No Entry in database!")

	def get_data_by_id(self,empid):
		print("got id: ",empid)
		try:
			mainlist = []
			cur = self.c.execute("SELECT * from EmpDB where emp_id=?",(empid,)) #storing data in a variable
			for row in cur:
				data_list = [row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12]]
			return data_list

			print("Operations performed successfully!")
		except Exception as e:
			print(e, "exception occured!!")
			print("No Entry in database!")

	def get_data_by_name(self,emp_name):
		print("got name: ",emp_name)
		try:
			mainlist = []
			cur = self.c.execute("SELECT * from EmpDB where name=?",(emp_name,)) #storing data in a variable
			for row in cur:
				data_list = [row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12]]
				mainlist.append(data_list)
			return mainlist

			print("Operations performed successfully!")
		except Exception as e:
			print(e, "exception occured!!")
			print("No Entry in database!")

	def get_specific_data(self, index):
		try:
			data_list=[]
			print("index: ",index)
	
			cur = self.c.execute('''SELECT * from EmpDB WHERE emp_id = ?''',(index,))
			for row in cur:
				data_list = [row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12]]
			print("data_list: ",data_list[1])
			return data_list
			print("Operations performed successfully!")
		except Exception as e:
			print(e, "exception occured!!")
			print("No Entry in database!")

	def update_database(self,empid, name, dob, addr, dept, desig, age, gender, sal, email, contact, joining, img):
		try:
			print("Updating.....")
			ageint = (int)(age)
			empidint = (int)(empid)
			salint = (int)(sal)
			tempimg = self.convertToBinaryData(img)
			print("after Updating.....")

			
			self.c.execute("UPDATE EmpDB set name=?,dob=?,address=?,department=?,designation=?,age=?,gender=?,salary=?,email=?,contact=?,joining=?,image=? where emp_id=?",(name, dob, addr, dept, desig, ageint, gender, salint, email, contact, joining, tempimg, empidint))
			self.conn.commit()
			print("Database updated successfully!")
			print("Total no. of rows updated: ", self.conn.total_changes)
	
		except Exception as e:
			print(e, "exception occured!!")
			print("No Entry in database!")


#closing the database
# creating_database()
# insert_data()
# c.close()
# conn.close()
